(*
 *
 * File: rt_floorf.st
 *
 * IEC 61131-3 Structured Text (ST) code generated for subsystem "fuzzControlSys/FuzCon"
 *
 * Model name                      : fuzzControlSys
 * Model version                   : 1.25
 * Model creator                   : konra
 * Model last modified by          : konra
 * Model last modified on          : Tue Nov 27 13:15:27 2018
 * Model sample time               : 0.05s
 * Subsystem name                  : fuzzControlSys/FuzCon
 * Subsystem sample time           : 0.05s
 * Simulink PLC Coder version      : 2.4 (R2017b) 24-Jul-2017
 * ST code generated on            : Mon Jan 07 17:31:24 2019
 *
 * Target IDE selection            : B&R Automation Studio 4.0
 * Test Bench included             : No
 *
 *)
FUNCTION rt_floorf
rt_floorf := DINT_TO_REAL(TRUNC(u));
IF u = rt_floorf THEN 
    rt_floorf := u;
ELSIF u < 0.0 THEN 
    rt_floorf := rt_floorf - 1.0;
END_IF;
END_FUNCTION
