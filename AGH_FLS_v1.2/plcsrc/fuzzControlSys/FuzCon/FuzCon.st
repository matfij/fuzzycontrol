(*
 *
 * File: FuzCon.st
 *
 * IEC 61131-3 Structured Text (ST) code generated for subsystem "fuzzControlSys/FuzCon"
 *
 * Model name                      : fuzzControlSys
 * Model version                   : 1.25
 * Model creator                   : konra
 * Model last modified by          : konra
 * Model last modified on          : Tue Nov 27 13:15:27 2018
 * Model sample time               : 0.05s
 * Subsystem name                  : fuzzControlSys/FuzCon
 * Subsystem sample time           : 0.05s
 * Simulink PLC Coder version      : 2.4 (R2017b) 24-Jul-2017
 * ST code generated on            : Mon Jan 07 17:31:24 2019
 *
 * Target IDE selection            : B&R Automation Studio 4.0
 * Test Bench included             : No
 *
 *)
FUNCTION_BLOCK FuzCon
CASE ssMethodType OF
    0: 
        (* InitializeConditions for Delay: '<S1>/Delay' *)
        Delay_DSTATE := 0.0;
        (* InitializeConditions for Delay: '<S1>/Delay1' *)
        Delay1_DSTATE := 0.0;
    1: 
        (* Product: '<S1>/Product' *)
        c_rtb_InterpolationUsingPre := Ke * In;
        (* Saturate: '<S2>/Saturation' *)
        IF c_rtb_InterpolationUsingPre >= 1.0 THEN 
            c_rtb_InterpolationUsingPre := 1.0;
        ELSIF  NOT (c_rtb_InterpolationUsingPre > 0.0) THEN 
            c_rtb_InterpolationUsingPre := 0.0;
        END_IF;
        (* End of Saturate: '<S2>/Saturation' *)
        
        (* PreLookup: '<S2>/Prelookup' *)
        (* Prelookup - Index and Fraction
           Index Search method: 'even'
           Extrapolation method: 'Linear'
           Use previous index: 'off'
           Use last breakpoint for index at or above upper limit: 'off'
           Remove protection against out-of-range input in generated code: 'off' *)
        IF c_rtb_InterpolationUsingPre <= 0.0 THEN 
            rtb_Prelookup_o1 := 0;
        ELSE 
            rtb_Prelookup_o1 := TRUNC(rt_floorf(u := c_rtb_InterpolationUsingPre * 62.9999962));
            c_rtb_InterpolationUsingPre := (c_rtb_InterpolationUsingPre - (INT_TO_REAL(rtb_Prelookup_o1) * 0.0158730168)) * 62.9999962;
        END_IF;
        (* End of PreLookup: '<S2>/Prelookup' *)
        
        (* Product: '<S1>/Product1' incorporates:
         *  Delay: '<S1>/Delay'
         *  Sum: '<S1>/Sum3' *)
        rtb_Sum := (In - Delay_DSTATE) * Kd;
        (* Saturate: '<S2>/Saturation1' *)
        IF rtb_Sum >= 1.0 THEN 
            rtb_Sum := 1.0;
        ELSIF  NOT (rtb_Sum > 0.0) THEN 
            rtb_Sum := 0.0;
        END_IF;
        (* End of Saturate: '<S2>/Saturation1' *)
        
        (* PreLookup: '<S2>/Prelookup1' *)
        (* Prelookup - Index and Fraction
           Index Search method: 'even'
           Extrapolation method: 'Linear'
           Use previous index: 'off'
           Use last breakpoint for index at or above upper limit: 'off'
           Remove protection against out-of-range input in generated code: 'off' *)
        IF rtb_Sum <= 0.0 THEN 
            rtb_Prelookup1_o1 := 0;
        ELSE 
            rtb_Prelookup1_o1 := TRUNC(rt_floorf(u := rtb_Sum * 62.9999962));
            rtb_Sum := (rtb_Sum - (INT_TO_REAL(rtb_Prelookup1_o1) * 0.0158730168)) * 62.9999962;
        END_IF;
        (* End of PreLookup: '<S2>/Prelookup1' *)
        
        (* Interpolation_n-D: '<S2>/Interpolation Using Prelookup' *)
        (* Interpolation 2-D
           Interpolation method: 'Linear'
           Use last breakpoint for index at or above upper limit: 'off'
           Overflow mode: 'wrapping' *)
        offset_1d := UDINT_TO_DINT((SHL(INT_TO_UDINT(rtb_Prelookup1_o1), 6)) + INT_TO_UDINT(rtb_Prelookup_o1));
        invSpc := ((c_InterpolationUsingPrelook[DINT_TO_UDINT(offset_1d) + 1] - c_InterpolationUsingPrelook[DINT_TO_UDINT(offset_1d)]) * c_rtb_InterpolationUsingPre) + c_InterpolationUsingPrelook[DINT_TO_UDINT(offset_1d)];
        c_rtb_InterpolationUsingPre := (((((c_InterpolationUsingPrelook[DINT_TO_UDINT(offset_1d) + 65] - c_InterpolationUsingPrelook[DINT_TO_UDINT(offset_1d) + 64]) * c_rtb_InterpolationUsingPre) + c_InterpolationUsingPrelook[DINT_TO_UDINT(offset_1d) + 64]) - invSpc) * rtb_Sum) + invSpc;
        (* Sum: '<S1>/Sum' incorporates:
         *  Delay: '<S1>/Delay1'
         *  Product: '<S1>/Product3' *)
        rtb_Sum := (beta * c_rtb_InterpolationUsingPre) + Delay1_DSTATE;
        (* Gain: '<S1>/Gain' *)
        Out :=  -sat;
        (* Switch: '<S3>/Switch' incorporates:
         *  Gain: '<S1>/Gain'
         *  RelationalOperator: '<S3>/UpperRelop' *)
        IF  NOT (rtb_Sum < ( -sat)) THEN 
            Out := rtb_Sum;
        END_IF;
        (* End of Switch: '<S3>/Switch' *)
        
        (* Switch: '<S3>/Switch2' incorporates:
         *  RelationalOperator: '<S3>/LowerRelop1' *)
        IF rtb_Sum > sat THEN 
            rtb_Sum := sat;
        ELSE 
            rtb_Sum := Out;
        END_IF;
        (* End of Switch: '<S3>/Switch2' *)
        
        (* Sum: '<S1>/Sum1' incorporates:
         *  Product: '<S1>/Product2' *)
        Out := (c_rtb_InterpolationUsingPre * alpha) + rtb_Sum;
        (* RelationalOperator: '<S4>/LowerRelop1' *)
        rtb_LowerRelop1_e := Out > limit[0];
        (* Switch: '<S4>/Switch' incorporates:
         *  RelationalOperator: '<S4>/UpperRelop' *)
        IF Out < limit[1] THEN 
            Out := limit[1];
        END_IF;
        (* End of Switch: '<S4>/Switch' *)
        
        (* Switch: '<S4>/Switch2' *)
        IF rtb_LowerRelop1_e THEN 
            Out := limit[0];
        END_IF;
        (* End of Switch: '<S4>/Switch2' *)
        
        (* Update for Delay: '<S1>/Delay' *)
        Delay_DSTATE := In;
        (* Update for Delay: '<S1>/Delay1' *)
        Delay1_DSTATE := rtb_Sum;
END_CASE;
END_FUNCTION_BLOCK
