function y = simfuzzy(u,fls,redMethod)
%{
    Uzyte zmienne:
    liczbaWejsc
    liczbaWyjsc
    LiczbaRegul
    rulein - macierz regul zwiazanych z wejsciowymi MF
    rulout - macierz regul zwiazanych z wyjsciowymi MF
    dolnaf - macierz gornych punktow zaplonu po rozmyciu wejscia
    gurnaf - macierz dolnych punktow zaplonu po rozmyciu wejscia
    dolnaaktywacja - wektor dolnych pnktow zaplonu po dzialaniu min/prod
    gornaaktywacja - wektor gornych pnktow zaplonu po dzialaniu min/prod
    dolnaFunkcja - dolna funkcja przynaleznosci zbiotu wyjsciowego
    gornaFunkcja - gorna funkcja przynaleznosci do zbioru rozmyteg
    dolnaSum - gorna funkcja centroidu
    gornaSum - dolna funkcja centroidu

%}
 %% wczytywanie regu�
    [~, liczbaWejsc] = size(fls.input);
    [~, liczbaWyjsc] = size(fls.output);
    [~, liczbaRegul] = size(fls.rule);
     
    rulein  = zeros(liczbaRegul,liczbaWejsc);
    ruleout = zeros(liczbaRegul,liczbaWyjsc);
     
    for i=1:liczbaRegul
       rulein(i,:)=fls.rule(i).antecedent;
       ruleout(i,:)=fls.rule(i).consequent; 
    end

%% rozmywanie wejscia
     dolnaf = zeros(liczbaRegul,liczbaWejsc);
     gornaf = dolnaf;
            
     for i=1:liczbaWejsc
         for j=1:liczbaRegul
             [dolna, gorna] = funkcjep(u(i), fls.input(i).mf(rulein(j,i)).params, fls.input(i).mf(rulein(j,i)).type);
             dolnaf(j,i) = dolna;
             gornaf(j,i) = gorna;
         end
     end
     clear dolna gorna;
     
%% okre�lenie stopnia aktywacji
   dolnaaktywacja = zeros(liczbaRegul,1);
   gornaaktywacja = dolnaaktywacja;
   
   if strcmp(fls.andMethod,'min')
       for i=1:liczbaRegul
           dolnaaktywacja(i) = min( dolnaf(i,:) );
           gornaaktywacja(i) = min( gornaf(i,:) );
       end    
   elseif strcmp(fls.andMethod,'prod') 
       for i=1:liczbaRegul
           dolnaaktywacja(i)=prod( dolnaf(i,:) );
           gornaaktywacja(i)=prod( dolnaf(i,:) );
       end 
   end

%% okre�lenie zbior�w wyj�ciowych
    r = fls.output.range;
    dane=r(1):0.01:r(2);
    clear r;
    y = zeros(1,liczbaWyjsc);

    %obliczenie wyjsc
    for q = 1:liczbaWyjsc 
        
        dolnaFunkcja = zeros(liczbaRegul,length(dane));
        gornaFunkcja = dolnaFunkcja;
        
        %Wyznaczenie gornej i dolnej funkcji przynaleznosci dla MF
        for j=1:liczbaRegul
            [dolna, gorna] = funkcjep(dane, fls.output(q).mf(ruleout(j,q)).params, fls.output(q).mf(ruleout(j,q)).type);
            
            dolna( dolna > dolnaaktywacja(j)) = dolnaaktywacja(j);
            gorna( gorna > gornaaktywacja(j)) = gornaaktywacja(j);
            dolnaFunkcja(j,:) = dolna;
            gornaFunkcja(j,:) = gorna;
        end
        clear dolna gorna;
        
        
%% sumowanie zbior�w wyj�ciowych
     
        if strcmp(fls.orMethod,'max')
            dolnaSum = max(dolnaFunkcja);
            gornaSum = max(gornaFunkcja);
            
        elseif strcmp(fls.orMethod,'prod') 
            dolnaSum = prod(dolnaFunkcja);
            gornaSum = prod(gornaFunkcja);
        end
     
%% redukcja typu
        if strcmp(redMethod,'KM')
            [L,~,~] = KM(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = KM(dane,dolnaSum,gornaSum,1); 
        elseif strcmp(redMethod,'BMM') 
            [L,~,~] = BMM(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = BMM(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'BMM') 
            [L,~,~] = BMM(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = BMM(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'CJ') 
            [L,~,~] = CJ(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = CJ(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'DY') 
            [L,~,~] = DY(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = DY(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'EIASC') 
            [L,~,~] = EIASC(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = EIASC(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'EIASC2') 
            [L,~,~] = EIASC2(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = EIASC2(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'EKM') 
            [L,~,~] = EKM(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = EKM(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'EKM2') 
            [L,~,~] = EKM2(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = EKM2(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'EODS') 
            [L,~,~] = EODS(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = EODS(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'G') 
            [L,~,~] = G(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = G(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'IASC') 
            [L,~,~] = IASC(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = IASC(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'LM') 
            [L,~,~] = LM(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = LM(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'LYZ') 
            [L,~,~] = LYZ(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = LYZ(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'NT') 
            [L,~,~] = NT(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = NT(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'TTCC') 
            [L,~,~] = TTCC(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = TTCC(dane,dolnaSum,gornaSum,1);
        elseif strcmp(redMethod,'WM') 
            [L,~,~] = WM(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = WM(dane,dolnaSum,gornaSum,1);
        else 
            [L,~,~] = KM(dane,dolnaSum,gornaSum,-1);
            [R,~,~] = KM(dane,dolnaSum,gornaSum,1);
        end
   
        %% defuzzyfikacja
        y(q)=(L+R)/2;
    end
