function [output] = controlSurface(fls,redMethod, n, f)
%CONTROLSURFACE tworzy wielowymiarowa tablice z sygnalem sterujacym
%   fls - struktura okreslajacy system logiczny
%   redMethod - metoda redukcji
%   n - rozdzielczosc tablicy
%   f - waitBar handle

%warunki poczatkowe
    switch nargin
        case 2
            n = 32;
        case 1
            n = 32;
            redMethod = 'KM';
        case 0
            warning('Not enough arguments');
            return;
    end
    
    if nargin ~= 4
        f = waitbar(0,'0/... (...)','CreateCancelBtn','setappdata(gcbf,''canceling'',1)');
    end
    
    
    [~, liczbaWejsc] = size(fls.input);
    [~, liczbaWyjsc] = size(fls.output);
    
    r = fls.output.range;
    dr = (r(2)-r(1))/(n-1);
    dane = r(1):dr:r(2);

    rozmiar = ones(1,liczbaWejsc)*n;
    output = zeros([rozmiar liczbaWyjsc]);
    
    %potrzebne do wyznaczenia pozostałego czasu
    tic;

	switch liczbaWejsc
        case 4
            for i1 = 1:n
                for i2 = 1:n
                    for i3 = 1:n
                       for i4 = 1:n
                           in = [dane(i1) dane(i2) dane(i3) dane(i4)];
                           output(i1,i2,i3,i4,:) = simfuzzy(in,fls,redMethod);
                       end
                   end
                end
                
                if getappdata(f,'canceling')
                    break
                end
                waitBar(f,i1,n)
           end
           
        case 3
           
        case 2
            for i1 = 1:n
                for i2 = 1:n
                	in = [dane(i1) dane(i2)];
                    output(i1,i2,:) = simfuzzy(in,fls,redMethod);
                end
                
                if getappdata(f,'canceling')
                    break
                end
                waitBar(f,i1,n)    
           end
           
       case 1
           
	end
   
   if nargin ~= 4
        delete(f);
   end
    
   output = single(output);    
end

function waitBar(f,x,n)
    timeLeft = toc/x*(n-x);
    
    str = sprintf('%0.1f%% (~%.2f s)',(x/n*100),timeLeft);
    waitbar(x/n,f,str);
end

