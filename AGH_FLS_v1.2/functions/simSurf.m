function [output] = simSurf(u,surf,r)
%SIMSURF Odczytuje wartosci wyjscia na podstawie wejsc
%   u - wektor wejscia
%   surf - tablica zawierajaca plaszczyzne sterowania
%   r - zakres wejsc i wyjsc zbiorow rozmytych
    
    if nargin == 2
        r = [0 1];
    end
    
    if length(size(surf)) > length(u)
        liczbaWyjsc = 2;
    else
        liczbaWyjsc = 1;
    end
    output = zeros(1,liczbaWyjsc);
    
    dr = (r(2)-r(1))/(length(surf)-1 );
    r = r(1):dr:r(2);
    
    
    
    switch length(u)
        case 1
            a = find( r >= u, 1);
            output = double(surf(a));
        case 2
            a1 = find( r >= u(1), 1);
            a2 = find( r >= u(2), 1);
            output = double(surf(a1, a2));
        case 3
            a1 = find( r >= u(1), 1);
            a2 = find( r >= u(2), 1);
            a3 = find( r >= u(3), 1);
            output = double(surf(a1, a2, a3));
        case 4
            a1 = find( r >= u(1), 1);
            a2 = find( r >= u(2), 1);
            a3 = find( r >= u(3), 1);
            a4 = find( r >= u(4), 1);
            
            
            output(:) = double(surf(a1, a2, a3, a4, :));
    end
    output = output/32767;
    

end

