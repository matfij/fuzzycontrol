function varargout = Edycja_wejsc(varargin)
% EDYCJA_WEJSC MATLAB code for Edycja_wejsc.fig
%      EDYCJA_WEJSC, by itself, creates a new EDYCJA_WEJSC or raises the existing
%      singleton*.
%
%      H = EDYCJA_WEJSC returns the handle to a new EDYCJA_WEJSC or the handle to
%      the existing singleton*.
%
%      EDYCJA_WEJSC('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in EDYCJA_WEJSC.M with the given input arguments.
%
%      EDYCJA_WEJSC('Property','Value',...) creates a new EDYCJA_WEJSC or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Edycja_wejsc_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Edycja_wejsc_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Edycja_wejsc

% Last Modified by GUIDE v2.5 31-Dec-2017 02:39:26

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Edycja_wejsc_OpeningFcn, ...
                   'gui_OutputFcn',  @Edycja_wejsc_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Edycja_wejsc is made visible.
function Edycja_wejsc_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Edycja_wejsc (see VARARGIN)

% Choose default command line output for Edycja_wejsc
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

%-------------Ustalenie wersji jezykowej----------------------------------
ten_jezyk = evalin('base','jezyk');
if ten_jezyk == 0
    
    nazwa = handles.nazwa;
    fls = readfis(nazwa);

    figure1_str=['Edycja wej��'];
    set(handles.figure1,'Name',figure1_str);

    Jezyk_str=['Jezyk'];
    set(handles.Jezyk,'Label',Jezyk_str);
        polski_str=['polski'];
        set(handles.polski,'Label',polski_str);
        angielski_str=['angielski'];
        set(handles.angielski,'Label',angielski_str);
    
end
if ten_jezyk == 1
    
    nazwa = handles.nazwa;
    fls = readfis(nazwa);

    figure1_str=['Edition inputs'];
    set(handles.figure1,'Name',figure1_str);

    Jezyk_str=['Language'];
    set(handles.Jezyk,'Label',Jezyk_str);
        polski_str=['Polish'];
        set(handles.polski,'Label',polski_str);
        angielski_str=['English'];
        set(handles.angielski,'Label',angielski_str);

    
end

% UIWAIT makes Edycja_wejsc wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Edycja_wejsc_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on selection change in edycja_wejsc.
function wej_ed_Callback(hObject, eventdata, handles)
% hObject    handle to edycja_wejsc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns edycja_wejsc contents as cell array
%        contents{get(hObject,'Value')} returns selected item from edycja_wejsc


% --- Executes during object creation, after setting all properties.
function wej_ed_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edycja_wejsc (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function naz_zmien_Callback(hObject, eventdata, handles)
% hObject    handle to naz_zmien (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of naz_zmien as text
%        str2double(get(hObject,'String')) returns contents of naz_zmien as a double


% --- Executes during object creation, after setting all properties.
function naz_zmien_CreateFcn(hObject, eventdata, handles)
% hObject    handle to naz_zmien (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function zak_Callback(hObject, eventdata, handles)
% hObject    handle to zak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of zak as text
%        str2double(get(hObject,'String')) returns contents of zak as a double


% --- Executes during object creation, after setting all properties.
function zak_CreateFcn(hObject, eventdata, handles)
% hObject    handle to zak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function wysw_zak_Callback(hObject, eventdata, handles)
% hObject    handle to wysw_zak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of wysw_zak as text
%        str2double(get(hObject,'String')) returns contents of wysw_zak as a double


% --- Executes during object creation, after setting all properties.
function wysw_zak_CreateFcn(hObject, eventdata, handles)
% hObject    handle to wysw_zak (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu2.
function popupmenu2_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu2


% --- Executes during object creation, after setting all properties.
function popupmenu2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --------------------------------------------------------------------
function Jezyk_Callback(hObject, eventdata, handles)
% hObject    handle to Jezyk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%------Wersja jezykowa polska------------------------------------------

function polski_Callback(hObject, eventdata, handles)
    
nazwa = handles.nazwa;
fls = readfis(nazwa);

figure1_str=['Edycja wej��'];
set(handles.figure1,'Name',figure1_str);

Jezyk_str=['Jezyk'];
set(handles.Jezyk,'Label',Jezyk_str);
    polski_str=['polski'];
    set(handles.polski,'Label',polski_str);
    angielski_str=['angielski'];
    set(handles.angielski,'Label',angielski_str);



% hObject    handle to polski (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%------Wersja jezykowa angielska------------------------------------------

function angielski_Callback(hObject, eventdata, handles)

nazwa = handles.nazwa;
fls = readfis(nazwa);

figure1_str=['Edition inputs'];
set(handles.figure1,'Name',figure1_str);
    
Jezyk_str=['Language'];
set(handles.Jezyk,'Label',Jezyk_str);
    polski_str=['Polish'];
    set(handles.polski,'Label',polski_str);
    angielski_str=['English'];
    set(handles.angielski,'Label',angielski_str);




% hObject    handle to angielski (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
