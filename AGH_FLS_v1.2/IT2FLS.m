function IT2FLS(varargin)
%IT2FLS
%   Funkcja sluzy do wywolywania toolboxa fuzzy 2
%   
%     Argumenty:
%     nazwa - nazwa aktualnego fis
%     nowyFls - pomocniczy arg. do utworzenia nowego fis
%     (B)varargin - argument wejsciowy/wyjsciowy
%     temp_val - pomocnicza wartosc 
%     temp_str - tymczasowa tablica stringow
%     str - string okreslony za pomoca 2 powyzszych arg.
%     I - macierz 0/1 okrelajaca identycznosc stringow
%     ind - indeks wartosci 1 w powyzszej tabeli
%     fls - zmienna w ktorej zapisany jest aktualny fis
%     klaw - arg. w ktorym zapisywany jest aktualnie wcisniety klawisz
%     plik - nazwa zapisywanego pliku
%     sciezka - sciezka dostepu do arg. plik
%     hObject - aktualny biekt
%     eventdata - nie uzywane
%     handles - struktura zawierajaca wszystkie obiekty i ich zmienne
%     
%     Funkcja zwraca:
%     varargin
%     jest to nazwa obecnego fis
%     
%     Uzywane funkcje:
%     Surf
%     Wej_ed
%     Pod_reg
%     Baza_regul
%     Wszystkie wywolania (Callback) do listbox, textbox, popup etc.
%     ktore pomimo ze sa zapisane w tym pliku jako osobne funkcje stanowia
%     integralna czesc tej funkcji poprzez plik IT2FLS.fig
%     
%     Uzywane zmienne:
%     varargin
%     
%     Uwagi:
%     Plik/funkcja mocno powiazana z plikiem IT2FLS.fig - w nim zapisana jest 
%     konfiguracja graficzna wygladu GUI a takze powiazanie funkcji callback
%     z ta funkcja.
%     
%     Autor:
%     Marcin Mik�as
%     rok 5
%     Kierunek: Automatyka i Robotyka
%     specjalnosc: Automatyka i Metrologia
%     Praca magisterska
%     temat: "Implementacja oprogramowania s�u��cego do badan nad regulatorami rozmytymi typu 2 "
%     Promotor:
%     dr inz. Ireneusz Dominik
% 
%     Ostatnia modyfikacja:
%     14.07.2014r.
%     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% 

gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @IT2FLS_OpeningFcn, ...
                   'gui_OutputFcn',  @IT2FLS_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
 if nargin && ischar(varargin{1})
     gui_State.gui_Callback = str2func(varargin{1});
 end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


%% OPENING FUNCTION


function IT2FLS_OpeningFcn(hObject, ~, handles, varargin)

    %dodanie wszystkich sciezek w folderze z wykluczeniami
    addpath(genpath(cd));
    rmpath(genpath('plcsrc'));
    rmpath(genpath('zzReszta'));
    
    
    % tworzenie obiektu wejscia i wyjscia w nowym gui
    if isempty(varargin)
        
        nazwa = 'untitled';
        handles.nazwa = nazwa;
        nowyFls = nowyflstypu2(nazwa);
        nowyFls = dodajzmiennatypu2(nowyFls,'input','input1',[0 1]);
        nowyFls = dodajzmiennatypu2(nowyFls,'input','input2',[0 1]);
        nowyFls = dodajzmiennatypu2(nowyFls,'output','output1',[0 1]);
        varargin = nowyFls;
        
        % Zapisanie danych fis w macierzy jako fls
        fls = varargin;
        
        %%%%%Wypisanie w polach - nazwa fis i typ fis prawdziwych danych
        set(handles.naz_fis_txt,'String',nazwa);
        set(handles.typ_fis_txt,'String',fls.type);
  
        %ilosc zsclices
        handles.zslices = 0;
        assignin('base','zslices',handles.zslices);
    
        %metoda redukcji
        handles.redMethod = 'EKM';
        assignin('base','redMethod',handles.redMethod);
        
        %jezyk
        handles.lang = 'pl';
        
        %odczytanie �cie�ki tego pliku
        handles.dir = cd;
        
        %Wczytanie danych z pol met_sum_pop,met_il_pop,wyn_pop,defuz_pop,zbier_pop  
        %met_sum_pop 
        temp_val = get(handles.met_sum_pop,'Value');
        temp_str = get(handles.met_sum_pop,'String');
        str = temp_str{temp_val};
        fls.andMethod = str;
   
        %met_il_pop
        temp_val = get(handles.met_il_pop,'Value');
        temp_str = get(handles.met_il_pop,'String');
        str = temp_str{temp_val};
        fls.orMethod = str;
    
        %wyn_pop
        temp_val = get(handles.wyn_pop,'Value');
        temp_str = get(handles.wyn_pop,'String');
        str = temp_str{temp_val};
        fls.impMethod = str;
    
        %defuz_pop
        cd( setflspath());
        %przeskanowac folder metody
        lista_metod = ls('Metody');
        lista_metod = cellstr(lista_metod);
        lista_metod = lista_metod(3:end);
        lista_metod = char(lista_metod);
        
        %%usunac niepotrzebna koncowke _tr.m
        for i = 1:size(lista_metod)
        
            for j = 1:length(lista_metod)
                if lista_metod(i,j) == '.'
                    lista_metod2(i,j-3:end) = ' ';
                break;
                else
                    lista_metod2(i,j) = lista_metod(i,j);
                end
            end
        end
        lista_metod = cellstr(lista_metod2);
    
        set(handles.defuz_pop,'String',lista_metod);
        temp_val = get(handles.defuz_pop,'Value');
        temp_str = get(handles.defuz_pop,'String');
        str = temp_str{temp_val};
   
        fls.defuzzMethod = str;
    
        %zbier_pop
        temp_val = get(handles.zbier_pop,'Value');
        temp_str = get(handles.zbier_pop,'String');
        str = temp_str{temp_val};

    else
        nazwa= varargin;
        nazwa = char(nazwa);
        fls = readfis(nazwa);
        handles.nazwa = nazwa;
        guidata(hObject,handles);
        fls.name = nazwa;
        set(handles.naz_fis_txt,'String','');
        
        %ustawianie wartosci okienek 
        set(handles.naz_fis_txt,'String',handles.nazwa);
        
        %met_sum_pop
        temp_str = get(handles.met_sum_pop,'String');
        I = strcmp(fls.andMethod,temp_str);
        ind = find(I);
        set(handles.met_sum_pop,'Value',ind);
        
        %met_il_pop
        temp_str = get(handles.met_il_pop,'String');
        I = strcmp(fls.orMethod,temp_str);
        ind = find(I);
        set(handles.met_il_pop,'Value',ind);
        
        %wyn_pop
        temp_str = get(handles.wyn_pop,'String');
        I = strcmp(fls.impMethod,temp_str);
        ind = find(I);
        set(handles.wyn_pop,'Value',ind); 
        
        %defuz_pop - skanuj folder w poszukiwaniu metod
        path = which('IT2FLS','-all');
        path_c = char(path);
        path_u = uint8(path_c);
        path_u = path_u(1:end-8);
        path_c = char(path_u);
        cd(path_c);
        temp_str = get(handles.defuz_pop,'String');
        I = strcmp(fls.defuzzMethod,temp_str);
        ind = find(I);
        set(handles.defuz_pop,'Value',ind);
        
        %zbier_pop
        temp_str = get(handles.zbier_pop,'String');
        I = strcmp(fls.aggMethod,temp_str);
        ind = find(I);
        set(handles.zbier_pop,'Value',ind);        
    end
      

    %zapisanie obecnego fis do tymczasowego pliku w celu odczytu w innym
    %gui
    writefis(fls,nazwa);
    guidata(hObject,handles);
    
    % Choose default command line output for IT2FLS
    handles.output = hObject;

    % Update handles structure
    guidata(hObject, handles);
    
    %wstawienie stringow do gui
    setGuiStrings(handles);

    % UIWAIT makes IT2FLS wait for user response (see UIRESUME)
    % uiwait(handles.glowne);

    %% OUTPUT FUNCTION
    
    
% --- Outputs from this function are returned to the command line.
function varargout = IT2FLS_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
%varargout{1} = handles.output;
handles.output = hObject;


%% 
%====================================
%%%%%Wygenerowane przez guide
% --- Executes on selection change in Wej_pop.
function Wej_pop_Callback(hObject, eventdata, handles)


% --- Executes during object creation, after setting all properties.
function Wej_pop_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in Wyj_pop.
function Wyj_pop_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function Wyj_pop_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on key press with focus on Wej_ed and none of its controls.
function Wej_ed_KeyPressFcn(hObject, eventdata, handles)


% --- Executes on button press in Wej_ed.
function Wej_ed_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Plik_gl_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Edycja_gl_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function Nowy_FIS_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Import_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Eksport_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Zamknij_gl_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Mamdami_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Sugeno_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Dodaj_zmienna_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Wejscie_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function Wyjscie_Callback(hObject, eventdata, handles)

% --------------------------------------------------------------------
function podglad_menu_Callback(hObject, eventdata, handles)

    
%% OKNA
% --------------------------------------------------------------------
function Plaszczyzna_Callback(hObject, eventdata, handles)
	surf_push_Callback(hObject, eventdata, handles);
    

% --------------------------------------------------------------------
function Pod_reg_Callback(~, ~, handles)
    Pod_reg(handles.nazwa, handles.lang)

function dodaj_zm_Callback(~, ~, handles)
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    writefis(fls,nazwa);
    
    Dodawanie_wejsc(nazwa, handles.lang);

    
% --- Executes on selection change in met_sum_pop.
function met_sum_pop_Callback(hObject, eventdata, handles)
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    str = get(handles.met_sum_pop,'String');
    val = get(handles.met_sum_pop,'Value');
    str = str{val};
    if strcmp(str,'min')
        fls.andMethod = str;
    else
        fls.andMethod = 'prod';
    end
    writefis(fls,nazwa);
    guidata(hObject,handles);


    %% POP UPY
% --- Executes during object creation, after setting all properties.
function met_sum_pop_CreateFcn(hObject, eventdata, handles)
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes on selection change in met_il_pop.
function met_il_pop_Callback(hObject, eventdata, handles)
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    str = get(handles.met_il_pop,'String');
    val = get(handles.met_il_pop,'Value');
    str = str{val};
    if strcmp(str,'max')
        fls.orMethod = str;
    else
        fls.orMethod = 'probor';
    end
    writefis(fls,nazwa);
    guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function met_il_pop_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in wyn_pop.
function wyn_pop_Callback(hObject, ~, handles)
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    str = get(handles.wyn_pop,'String');
    val = get(handles.wyn_pop,'Value');
    str = str{val};
    if strcmp(str,'min')
        fls.impMethod = str;
    else
        fls.impMethod = 'prod';
    end
    writefis(fls,nazwa);
    guidata(hObject,handles);

    
% --- Executes during object creation, after setting all properties.
function wyn_pop_CreateFcn(hObject, ~, ~)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in zbier_pop.
function zbier_pop_Callback(hObject, ~, handles)
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    str = get(handles.zbier_pop,'String');
    val = get(handles.zbier_pop,'Value');
    str = str{val};
    if strcmp(str,'max')
        fls.aggMethod = str;
    else
        fls.aggMethod = 'probor';
    end
    writefis(fls,nazwa);
    guidata(hObject,handles);


% --- Executes during object creation, after setting all properties.
function zbier_pop_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in defuz_pop.
function defuz_pop_Callback(hObject, eventdata, handles)
nazwa = handles.nazwa;
    fls = readfis(nazwa);
    
    str = get(handles.defuz_pop,'String');
    val = get(handles.defuz_pop,'Value');
    
    str = str{val};
    fls.defuzzMethod = str;
 
    writefis(fls,nazwa);
    guidata(hObject,handles);
  
%% POLA TEKSTOWE

 function naz_fis_txt_Callback(hObject, eventdata, handles)
    nazwa = handles.nazwa;
    fls = readfis(nazwa); 
    klaw = get(gcf, 'CurrentKey');
  
    if strcmp(klaw,'return')
       
       fls_name = get(handles.naz_fis_txt,'String');
       set(handles.naz_fis_txt,'String',fls_name);
       fls.name = fls_name;
       handles.nazwa = fls_name;
       writefis(fls,fls_name);
       
       guidata(hObject,handles);
    else
        return;
    end


% --- Executes during object creation, after setting all properties.
function naz_fis_txt_CreateFcn(hObject, eventdata, handles)
% hObject    handle to naz_fis_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
   
    
    
% --- Executes during object creation, after setting all properties.
function defuz_pop_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in wejscia_pop.
function wejscia_pop_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function wejscia_pop_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in wyjscia_pop.
function wyjscia_pop_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function wyjscia_pop_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function naz_zm_main_Callback(hObject, eventdata, handles)
% hObject    handle to naz_zm_main (see GCBO)
hndl=findobj(obecnyWyk,'Type','uicontrol','Tag','naz_zm_main');
    set(hndl,'String',' ');

% --- Executes during object creation, after setting all properties.
function naz_zm_main_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function typ_zm_main_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function typ_zm_main_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function zak_zm_main_Callback(hObject, eventdata, handles)

% --- Executes during object creation, after setting all properties.
function zak_zm_main_CreateFcn(hObject, eventdata, handles)

if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% BUTTONY

% --- Executes on button press in info.
function info_Callback(hObject, eventdata, handles)
    msgbox(getLangString(70,handles.lang),getLangString(71,handles.lang));


% --- Executes on button press in Zamknij_gl.
function wyjscie_all_Callback(hObject, eventdata, handles)
    glowne_CloseRequestFcn(hObject, eventdata, handles);

                    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%% Zamykanie Gui
%%%%% Executes when user attempts to close glowne.
function glowne_CloseRequestFcn(~, ~ , handles)
 try
    lang = handles.lang;
 catch
     lang = 'pl';
 end
    pytanie = getLangString(72,lang);
    zgoda = getLangString(73,lang);
    niezgoda = getLangString(74,lang);
    odrzucenie = getLangString(75,lang);
    title = getLangString(76,lang);
    
    
    selection = questdlg(pytanie, title, zgoda, niezgoda, odrzucenie, odrzucenie);

    switch selection
        case zgoda
            Do_pliku_Callback(0,0,handles)
            delete(gcbo);
            
        case niezgoda
            delete(gcbo);
            
        case odrzucenie
            return;
    end

%% import
% --------------------------------------------------------------------
function Z_pliku_Callback(~, ~, handles)
    try    
        [FILENAME,PATHNAME] = uigetfile('*.mat','Wczytaj plik');
        
        if ~(isequal(FILENAME,0) || isequal(PATHNAME,0))
            fls = importdata(FILENAME);
            writefis(fls,fls.name);
            fls.name = char(fls.name);
            
            msgbox([getLangString(77,handles.lang), FILENAME]);
        end
    catch
        msgbox(getLangString(78,handles.lang), 'Error','error');
    end
    
% --------------------------------------------------------------------
function Z_workspace_Callback(~, ~, handles)  
    try
        fls = evalin('base','fls');
        lang = handles.lang;
        writefis(fls,fls.name);
          
        ilosc_wej_str = [getLangString(30,lang), num2str(length(fls.input ))];
        ilosc_wyj_str = [getLangString(31,lang), num2str(length(fls.output))];
        ilosc_reg_str = [getLangString(46,lang), num2str(length(fls.rule))];
        set(handles.ilosc_wej_txt,'String',ilosc_wej_str); 
        set(handles.ilosc_wyj_txt,'String',ilosc_wyj_str);
        set(handles.text26,'String',ilosc_reg_str);
        
        msgbox(getLangString(79,handles.lang));
    catch
        msgbox(getLangString(78,handles.lang), 'Error','error');
    end

%% eksport
% --------------------------------------------------------------------
function Do_pliku_Callback(~, ~, handles)
    nazwa = handles.nazwa;
    
    try 
        fls = readfis(nazwa);
        assignin('base','fls',fls);
        
        [FILENAME,PATHNAME] = uiputfile('*.mat','Zapisz jako');
        if ~(isequal(FILENAME,0) || isequal(PATHNAME,0))
            save(FILENAME,'fls');
            
            msgbox([getLangString(80,handles.lang), FILENAME]);
        end
    catch
         msgbox(getLangString(81,handles.lang));
    end


% --------------------------------------------------------------------
function Do_workspace_Callback(~, ~, handles)
    try
        fls = readfis(handles.nazwa);
        assignin('base','fls',fls);
        
        msgbox(getLangString(82,handles.lang));
    catch
        msgbox(getLangString(81,handles.lang));
    end

    
% --- Executes on button press in wej_push.
function wej_push_Callback(hObject, eventdata, handles)
    Wej_ed(handles.nazwa, handles.lang);



% --- Executes on button press in baza_push.
function baza_push_Callback(hObject, eventdata, handles)
    Baza_regul(handles.nazwa, handles.lang);



% --- Executes on button press in wyj_push.
function wyj_push_Callback(hObject, eventdata, handles)
    handles = guidata(hObject);
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    Wej_ed(nazwa);



% --- Executes on button press in podg_push.
function podg_push_Callback(hObject, eventdata, handles)
    Pod_reg(handles.nazwa, handles.lang);



% --- Executes on button press in surf_push -- shows ontrol surface
function surf_push_Callback(hObject, eventdata, handles)
    fls = readfis(handles.nazwa);
    if isempty(fls.rule)        % rules exist?
        msgbox(getLangString(47,handles.lang), ' ','warn');
    elseif length(fls.input) ~= 2 || length(fls.output) ~=1 %only two input one output
        msgbox(getLangString(48,handles.lang), ' ','warn');
    else 
        
        %to okno jest do naprawy
        %�le wy�wielta 
        %Surf(handles.nazwa, handles.lang);
        
        figure;
        mesh(controlSurface(fls, handles.redMethod,32));
    end
    


% --------------------------------------------------------------------
function defuz_dodaj_Callback(hObject, eventdata, handles)
        handles = guidata(hObject);
        nazwa = handles.nazwa;
        fls = readfis(nazwa);
        writefis(fls,nazwa);
        Dodawanie_funkcji(handles.nazwa, handles.lang);


% --- Executes on button press in general_ed.
function general_ed_Callback(hObject, eventdata, handles)
        handles = guidata(hObject);
        nazwa = handles.nazwa;
        fls = readfis(nazwa);
        writefis(fls,nazwa);        
        General_ed(handles.nazwa, handles.lang);   



% --- Executes on button press in general_on.
function general_on_Callback(hObject, eventdata, handles)
        handles = guidata(hObject);
        nazwa = handles.nazwa;
        fls = readfis(nazwa);
        writefis(fls,nazwa);
        handles.zslices = evalin('base','zslices'); 
        fls.general = get(handles.general_on,'Value');
        if fls.general == 1
            if handles.zslices == 0
            set(handles.general_ed,'Enable','On');
            set(handles.general_ed,'FontWeight','bold'); 
            handles.zslices = 3;
            assignin('base','zslices',handles.zslices);
            end
        else
            set(handles.general_ed,'Enable','Off');   
            set(handles.general_ed,'FontWeight','normal');
            handles.zslices = 0;
            assignin('base','zslices',handles.zslices);
        end
         guidata(hObject,handles);
         


% --- Executes during object creation, after setting all properties.
function glowne_CreateFcn(hObject, eventdata, handles)
% hObject    handle to glowne (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% --- Executes on selection change in popupmenu10.
function popupmenu10_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu10 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu10


% --- Executes during object creation, after setting all properties.
function popupmenu10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

list = dir('metody_redukcji');
names = {list.name};
names = names(3:end);
clear list;

set(hObject, 'String', names);

handles.names = names;
guidata(hObject, handles);


% --- Executes on button press in pushbutton13.
function pushbutton13_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton13 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
index = get(handles.popupmenu10, 'Value');
list = handles.names;
name = list{index};

%zapisanie metody redukcji

%redMethod = evalin('base','redMethod');
handles.redMethod = name(6:(end-2));
assignin('base','redMethod',handles.redMethod);

%---------------redukcja BR------------------------------------------

nazwa = handles.nazwa;
fls = readfis(nazwa);
stringu = get(handles.popupmenu10,'String');
valu = get(handles.popupmenu10,'Value');
stringu = stringu{valu};


if strcmp(stringu,'adaptBMM.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptCJ.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptDY.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptEIASC.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptEIASC2.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptEKM.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptEKM2.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptEODS.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptG.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptIASC.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptKM.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptLM.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptLYZ.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptNT.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptTTCC.m')
fls.reductionMethod = stringu;
elseif strcmp(stringu,'adaptWM.m')
fls.reductionMethod = stringu;
else
    fls.reductionMethod = 'adaptCJ';
end
 writefis(fls,nazwa);
 
 %-------------------------------------------------------------------
 
copyfile(strcat('metody_redukcji\', name), 'Operacje\adapt.m');
guidata(hObject, handles);

% --- Executes during object deletion, before destroying properties.
function ilosc_wyj_txt_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to ilosc_wyj_txt (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function Jezyk_Callback(hObject, eventdata, handles)
% hObject    handle to Jezyk (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)



%%  Wybor jezyka
%	Wersja jezykowa angielska
function angielski_Callback(hObject, ~, handles)
    handles.lang = 'en';
    setGuiStrings(handles);
    
    guidata(hObject,handles);

%	Wersja jezykowa polska
function polski_Callback(hObject, ~, handles)
    handles.lang = 'pl';
    setGuiStrings(handles);
    
    guidata(hObject,handles);
  
    
%%   F-cja podstawiajaca stringi
function setGuiStrings(handles)
    nazwa = handles.nazwa;
    lang = handles.lang;
    fls = readfis(nazwa);
    
    set(handles.Jezyk,'Label',getLangString(10,lang));
    set(handles.polski,'Label',getLangString(11,lang));
    set(handles.angielski,'Label',getLangString(13,lang));
    set(handles.podglad_menu,'Label',getLangString(14,lang));
    set(handles.Plaszczyzna,'Label',getLangString(15,lang));
    set(handles.Pod_reg,'Label',getLangString(16,lang));
    set(handles.Edycja_gl,'Label',getLangString(17,lang));
    
    set(handles.dodaj_zm,'Label',getLangString(18,lang));
    set(handles.Plik_gl,'Label',getLangString(19,lang));
    set(handles.Import,'Label',getLangString(20,lang));
    set(handles.Z_workspace,'Label',getLangString(21,lang));
    set(handles.Z_pliku,'Label',getLangString(19,lang));
    set(handles.Eksport,'Label',getLangString(23,lang));
    set(handles.Do_workspace,'Label',getLangString(44,lang));
    set(handles.Do_pliku,'Label',getLangString(45,lang));
    set(handles.defuz_dodaj,'Label',getLangString(24,lang));
    set(handles.Zamknij_gl,'Label',getLangString(25,lang));
    set(handles.podg_push,'String',getLangString(16,lang));
    set(handles.general_ed,'String',getLangString(26,lang));
    set(handles.surf_push,'String',getLangString(27,lang));
    set(handles.wej_push,'String',getLangString(28,lang));
    set(handles.baza_push,'String',getLangString(29,lang));

    ilosc_wej_str = [getLangString(30,lang), num2str(length(fls.input ))];
    ilosc_wyj_str = [getLangString(31,lang), num2str(length(fls.output))];
    ilosc_reg_str = [getLangString(46,lang), num2str(length(fls.rule))];
    set(handles.ilosc_wej_txt,'String',ilosc_wej_str); 
    set(handles.ilosc_wyj_txt,'String',ilosc_wyj_str);
    set(handles.text26,'String',ilosc_reg_str);

    set(handles.uipanel9,'Title',getLangString(32,lang));
    set(handles.text6,'String',getLangString(33,lang));
    set(handles.text7,'String',getLangString(34,lang));
    set(handles.text8,'String',getLangString(35,lang));
    set(handles.text9,'String',getLangString(36,lang));
    set(handles.text22,'String',getLangString(37,lang));
    set(handles.text12,'String',getLangString(38,lang));
    
    set(handles.uipanel10,'Title',getLangString(39,lang));
    set(handles.naz_fis,'String',getLangString(40,lang));
    set(handles.text15,'String',getLangString(41,lang));
    set(handles.text21,'String',getLangString(42,lang));
    
    set(handles.info,'String',getLangString(43,lang));
    set(handles.wyjscie_all,'String',getLangString(25,lang));
    
    set(handles.pushbutton14,'String',getLangString(83,lang));

% --------------------------------------------------------------------
function Untitled_1_Callback(hObject, eventdata, handles)
% hObject    handle to Untitled_1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on key press with focus on surf_push and none of its controls.
function surf_push_KeyPressFcn(hObject, eventdata, handles)
% hObject    handle to surf_push (see GCBO)
% eventdata  structure with the following fields (see MATLAB.UI.CONTROL.UICONTROL)
%	Key: name of the key that was pressed, in lower case
%	Character: character interpretation of the key(s) that was pressed
%	Modifier: name(s) of the modifier key(s) (i.e., control, shift) pressed
% handles    structure with handles and user data (see GUIDATA)


% --- If Enable == 'on', executes on mouse press in 5 pixel border.
% --- Otherwise, executes on mouse press in 5 pixel border or over surf_push.
function surf_push_ButtonDownFcn(hObject, eventdata, handles)
% hObject    handle to surf_push (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


%% Przycisk do generacji kodu do AS
function pushbutton14_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton14 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

     codeGenerationAS(handles.nazwa, handles.lang, handles.redMethod);
     
