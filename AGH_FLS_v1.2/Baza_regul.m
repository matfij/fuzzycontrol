function varargout = Baza_regul(varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Baza_regul
%   Funkcja sluzy do wywolywania okna tworzenia regul
%   
%     Argumenty:
%     (I)varargin - argument wejsciowy
%     (O)vargout - argument wyjsciowy
%
%     Funkcja zwraca:
%     varargout
%     jest to nazwa obecnego fis
%     
%     Uzywane funkcje:
%     IT2FLS
%     + funkcje wbudowane w program Matlab
%
%     Uzywane zmienne:
%     nazwa - nazwa aktualnego fis
%     IleWejsc
%     IleWyjsc
%     Col
%     mfLt
%     listbox1
%     listbox2
%     num
%     zas_Num
%     Id_wej
%     Id_wyj
%     str1
%     str2
%     str3
%     temp_val
%     temp_str
%     baz_Reg_str
%     old_baz_reg_str
%     (I)varargin - argument wejsciowy
%     (O)vargout - argument wyjsciowy
%     fls - zmienna w ktorej zapisany jest aktualny fis
%     klaw - arg. w ktorym zapisywany jest aktualnie wcisniety klawisz
%     plik - nazwa zapisywanego pliku
%     sciezka - sciezka dostepu do arg. plik
%     hObject - aktualny obiekt
%     eventdata - nie uzywane
%     handles - struktura zawierajaca wszystkie obiekty i ich zmienne
%     Wszystkie wywolania (Callback) do listbox, textbox, popup etc.
%     ktore pomimo ze sa zapisane w tym pliku jako osobne funkcje stanowia
%     integralna czesc tej funkcji poprzez plik IT2FLS.fig
%     
%     Uwagi:
%     Plik/funkcja mocno powiazana z plikiem Baza_regul.fig - w nim zapisana jest 
%     konfiguracja graficzna wygladu GUI a takze powiazanie wszystkich funkcji callback
%     z ta funkcja.
%     
%     Autor:
%     Marcin Mik�as
%     rok 4
%     specjalnosc: Automatyka i Robotyka
%     Praca in�ynierska
%     temat: "Praktyczna implementacja regulatora rozmytego typu 2 w oprogramowaniu Matlab"
%     Promotor:
%     dr inz. Ireneusz Dominik
% 
%     Ostatnia modyfikacja:
%     03.01.2013r.
%     
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Baza_regul_OpeningFcn, ...
                   'gui_OutputFcn',  @Baza_regul_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before Baza_regul is made visible.
function Baza_regul_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Baza_regul (see VARARGIN)


% Choose default command line output for Wej_ed
handles.output = hObject;
%structure = findobj(IT2FLS); 


%obsluga argumentow
if nargin > 0
    if ~isempty(varargin(1))
        nazwa = char(varargin(1));
        fls = readfis(nazwa);
        handles.nazwa = nazwa;
    else
        fls = readfis('bez_nazwy');
        handles.nazwa = 'bez_nazwy';
        nazwa = handles.nazwa;
    end
else
    fls = readfis('bez_nazwy');
    handles.nazwa = 'bez_nazwy';
    nazwa = handles.nazwa;    
end

if nargin > 1
    if ~isempty(varargin(2))
        lang = char(varargin(2));
        handles.lang = lang;
    else
        lang = 'pl';
        handles.lang = lang;
    end
else
    lang = 'pl';
    handles.lang = lang;
end


set(handles.figure1,'Name',getLangString(220,lang));
i_text      = getLangString(226,lang);
jezeli_text = getLangString(227,lang);
jest_text   = getLangString(228,lang);
wtedy_text  = getLangString(229,lang);


IleWejsc = length(fls.input);
IleWyjsc = length(fls.output);
Col = [0.99 1 0.7];

%napisy nad okienkami
WeTxt = uicontrol('Style','text', ...
                   'String',getLangString(221,lang), ...
                   'BackgroundColor',Col, ...
                   'Position', [0 180 100 20]);
                 
WeTxt = uicontrol('Style','text', ...
                   'String',getLangString(222,lang), ...
                   'BackgroundColor',Col, ...
                   'Position', [450 180 100 20]);

               
%przycisk dodawania zasady
set(handles.pushbutton1, 'String', getLangString(224,lang));
% DodBut = uicontrol('Style','pushbutton', ...
%                    'String',getLangString(224,lang), ...
%                    'BackgroundColor', Col, ...
%                    'Position', [130 10 80 30], ...
%                    'Callback', @dodzasfcn);

%przycisk usuniecia zasady
UsBut = uicontrol('Style','pushbutton', ...
                  'String',getLangString(225,lang), ...
                  'BackgroundColor', Col, ...
                  'Position', [330 10 80 30], ...
                  'Callback', @uszasfcn);
                      
%update handles w gui
guidata(hObject, handles);
                      
  
      
%petle tworzaca odpowiednia ilosc tabel wejsc i wyjsc pozwalajacych na
%edycje
%ustawienia dla elementow
%pierwotne handles okienek wejsc
WeLiBox1 = 0;
WeLiBox2 = 0;
WeLiBox3 = 0;
WeLiBox4 = 0;
WyLiBox1 = 0;
WyLiBox2 = 0;
    
    
for i = 1:IleWejsc
    %przygotowanie tekstu do listboxa
    mfLt = length(fls.input(i).mf);
    for j = 1:mfLt
        if j == 1
            lbTxt = cellstr('');
        end
            lbTxt = fls.input(i).mf(j).name;
            lbTxt = cellstr(lbTxt);
        switch i 
            case 1
                listboxTxt1(j) = lbTxt;
                listboxTxt1 = listboxTxt1';
            case 2
                listboxTxt2(j) = lbTxt;
                listboxTxt2 = listboxTxt2';
            case 3
                listboxTxt3(j) = lbTxt;
                listboxTxt3 = listboxTxt3';
            case 4
                listboxTxt4(j) = lbTxt;
                listboxTxt4 = listboxTxt4';
        end
    end
    %listbox
    num = num2str(i);
    nazwa = ['WeLiBox' num];
    listbox = ['listboxTxt' num];
    WeLiBox1 = uicontrol('Style','Listbox', ...
        'Min', 1, ...
        'Max', 1, ...
        'Tag',nazwa, ...
        'String',listboxTxt1, ...
        'Value',1,...
        'Position', [0 50 80 100]);
    
    if i >= 2
        WeLiBox2 = uicontrol('Style','Listbox', ...
            'Min', 1, ...
            'Max', 1, ...
            'Tag',nazwa, ...
            'String',listboxTxt2, ...
            'Value',1,...
            'Position', [90 50 80 100]);
    end
    
    %warunkowe dodanie 3 okna - 3 wysjcie
    if i >= 3
        WeLiBox3 = uicontrol('Style','Listbox', ...
            'Min', 1, ...
            'Max', 1, ...
            'Tag',nazwa, ...
            'String',listboxTxt3, ...
            'Value',1,...
            'Position', [180 50 80 100]);                     
    end
    
    %warunkowe dodanie 4 okna - 4 wysjcie
    if i == 4
        WeLiBox4 = uicontrol('Style','Listbox', ...
            'Min', 1, ...
            'Max', 1, ...
            'Tag',nazwa, ...
            'String',listboxTxt4, ...
            'Value',1,...
            'Position', [270 50 80 100]);                     
    end 

    %txt
    WeTxt(i) = uicontrol('Style','text', ...
        'String',[ fls.input(i).name ' jest...'], ...
        'BackgroundColor',[0.99 1 0.7], ...
        'Position', [90*(i-1) 150 80 20]);
end

    %update handles w gui
    handles.WeLiBox1 = WeLiBox1;
    handles.WeLiBox2 = WeLiBox2;
    handles.WeLiBox3 = WeLiBox3;
    handles.WeLiBox4 = WeLiBox4;
    guidata(hObject,handles);

    
    for i = 1:IleWyjsc
        %przygotowanie tekstu do listboxa
        mfLt = length(fls.output(i).mf);
        for j = 1:mfLt
            lbTxt = fls.output(i).mf(j).name;
            lbTxt = cellstr(lbTxt);
            listboxTxt(j) = lbTxt;
        end
        listboxTxt = listboxTxt';
         
        %listbox
        num = num2str(i);
        nazwa = ['WyLiBox' num];
        WyLiBox(i) = uicontrol('Style','Listbox', ...
            'Min', 1, ...
            'Max', 1, ...
            'Tag',nazwa, ...
            'String',listboxTxt, ...
            'Position', [450-90*(i-1) 50 80 100]);
        %txt
        WyTxt(i) = uicontrol('Style','text', ...
             'String',fls.output(i).name, ...
             'BackgroundColor',[0.99 1 0.7], ...
             'Position', [450-90*(i-1) 150 80 20]);
    end
    
    %update handles w gui
    handles.WyLiBox1 = WyLiBox(1);
    if IleWyjsc ==2
        handles.WyLiBox2 = WyLiBox(2);
    end
    guidata(hObject,handles);
    
    handles.fls = fls;

    %kasowanie aktualnego stanu listbox1
    set(handles.listbox1,'String','');   
    %wczytanie stanu bazy z pliku fls
    %zebranie id kazdego z wynikow wybranych w bazie regul
	if numel(fls.rule) >= 1
        zas_Num = length(fls.rule);

            for i = 1:zas_Num
                    Id_wej(i,:) = fls.rule(i).antecedent;
                    Id_wyj(i,:) = fls.rule(i).consequent;
                    Conn(i) = fls.rule(i).connection;
              
                for j = 1:IleWejsc
                    set(handles.listbox1,'Value',Id_wej(i,j));
                end
                for j = 1:IleWyjsc
                    set(handles.listbox1,'Value',Id_wyj(i,j));
                end
                
            end
            
           
            for i = 1:zas_Num
                temp_str = get(handles.WeLiBox1,'String');
                    if Id_wej(i,1) == 0
%                         str1 = 'nic';
                    else
                        str1 = temp_str{Id_wej(i,1)};
                    end
                if IleWejsc >= 2    
                temp_str = get(handles.WeLiBox2,'String');
                    if Id_wej(i,2) == 0
%                         str2 = 'nic';
                    else
                        str2 = temp_str{Id_wej(i,2)}; 
                    end
                end
                %jezeli jest 3 wejscie
                if IleWejsc >= 3    
                temp_str = get(handles.WeLiBox3,'String');
                    if Id_wej(i,3) == 0
%                         str3 = 'nic';
                    else
                        str3 = temp_str{Id_wej(i,3)};
                    end
                end
                %jezeli jest 4 wejscie    
                if IleWejsc == 4
                temp_str = get(handles.WeLiBox4,'String');
                    if Id_wej(i,4) == 0
%                         str4 = 'nic';
                    else
                        str4 = temp_str{Id_wej(i,4)};
                    end
                end
                %wyjscia
                    temp_str = get(handles.WyLiBox1,'String');
                    str5 = temp_str{Id_wyj(i,1)};
                if IleWyjsc == 2
                    temp_str = get(handles.WyLiBox2,'String');
                    str6 = temp_str{Id_wyj(i,2)};
                end

%generacja stringow  do listbox1
                if strcmp(str1,'nic')
                               str_part1 = [];
               else
                               str_part1 = ([ fls.input(1).name jest_text str1]);

                              
               end
              if IleWejsc < 2 || strcmp(str2,'nic')
                               str_part2 = [];
                               str_part1_con = [];
              else
                               str_part2 = ([fls.input(2).name jest_text str2]);
                             if Conn(i) == 1
                                 str_part1_con = i_text;
                             elseif Conn(i) == 2
                                 str_part1_con = '" lub "';
                             end
              end           
  
              if IleWejsc < 3 || strcmp(str3,'nic')
                               str_part3 = [];
                               str_part2_con = [];
              else
                               str_part3 = ([fls.input(3).name jest_text str3]);
                             if Conn(i) == 1
                                 str_part2_con = i_text;
                             elseif Conn(i) == 2
                                 str_part2_con = '" lub "';
                             end
              end
                             
              if IleWejsc < 4 || strcmp(str4,'nic')
                               str_part4 = [];
                               str_part3_con = [];
              else
                               str_part4 = ([fls.input(4).name ' "' jest_text '" ' str4]);
                              if Conn(i) == 1
                                 str_part3_con = i_text;
                             elseif Conn(i) == 2
                                 str_part3_con = '" lub "';
                             end

              end
              
              if strcmp(str5,'nic')
                               str_part5 = [];
              else
                               str_part5 = ([ fls.output(1).name ' "' jest_text '" ' str5 ' "']);
              end
              
              if IleWyjsc ==2 
                               if strcmp(str6,'nic')
                                   str_part6 = [];
                               else
                               str_part6 = ([fls.output(2).name jest_text str6]);
                               end
                               
              else             
                               str_part6 = ([]);
              end
                            

                   baz_reg_txt = ([jezeli_text str_part1 str_part1_con str_part2 str_part2_con str_part3 str_part3_con str_part4 wtedy_text  str_part5 str_part6]);

                baz_reg_str(i) = cellstr(baz_reg_txt);
                set(handles.listbox1,'Value',length(baz_reg_str));
                set(handles.listbox1,'String',baz_reg_str);
            end
	end

% Update handles structure
guidata(hObject, handles);

    
% function edzasfcn(hObject, eventdata, handles)  
%     handles = guidata(hObject);
%     nazwa = handles.nazwa;
%     fls = readfis(nazwa);
%     IleWejsc = length(fls.input);
%     IleWyjsc = length(fls.output);
%     handles = guidata(hObject);

      
function uszasfcn(hObject, eventdata, handles)
    handles = guidata(hObject);
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    handles = guidata(hObject);

    wybrana_reg_num = get(handles.listbox1,'Value');
    baz_reg_str = get(handles.listbox1,'String');
    baz_reg_str(wybrana_reg_num) = [];

    %wpisanie wyedytowanego stringa
    set(handles.listbox1,'Value',length(baz_reg_str));
    set(handles.listbox1,'String',baz_reg_str);
    fls.rule(wybrana_reg_num) = [];

    writefis(fls,nazwa);

    guidata(hObject, handles);
      

% --- Outputs from this function are returned to the command line.
function varargout = Baza_regul_OutputFcn(hObject, eventdata, handles) 
    varargout{1} = handles.output;


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
    if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
        set(hObject,'BackgroundColor','white');
    end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)



% --- Executes on key press with focus on listbox1 and none of its controls.
function listbox1_KeyPressFcn(hObject, eventdata, handles)


% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    writefis(fls,nazwa);
    delete(gcf);

    IT2FLS(handles.nazwa);



% --- Executes when selected object is changed in uipanel1.
function uipanel1_SelectionChangeFcn(hObject, eventdata, handles)
% hObject    handle to the selected object in uipanel1 
% eventdata  structure with the following fields (see UIBUTTONGROUP)
%	EventName: string 'SelectionChanged' (read only)
%	OldValue: handle of the previously selected object or empty if none was selected
%	NewValue: handle of the currently selected object
% handles    structure with handles and user data (see GUIDATA)


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, ~, handles)
    lang = handles.lang;
    
    i_text      = getLangString(226,lang);
    jezeli_text = getLangString(227,lang);
    jest_text   = getLangString(228,lang);
    wtedy_text  = getLangString(229,lang);

        
    handles = guidata(hObject);
    handles2 = get(handles.uipanel1);
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    IleWejsc = length(fls.input);
    IleWyjsc = length(fls.output);
    handles = guidata(hObject);
    
    %dodanie warunku wyboru polaczenia
    if handles2.SelectedObject==handles.radiobutton1
        Conn = 1;
    else
        Conn = 2;
    end

    %pobranie wartosci z okna wejsc
    lb1_dl = length(get(handles.listbox1,'String'));  
    if IleWejsc >= 1
        %wybranie aktualnie kliknietego
        temp_val1 = get(handles.WeLiBox1,'Value');
        temp_str = get(handles.WeLiBox1,'String');
        str1 = temp_str{temp_val1};
        dodaj_ok = 1;
    end
    %wybranie aktualnie kliknietego - input2
    if IleWejsc >= 2
        temp_val2 = get(handles.WeLiBox2,'Value');
        temp_str = get(handles.WeLiBox2,'String');
        str2 = temp_str{temp_val2};

        if strcmp(str2,'nic')
        	temp_val2 = 0;
        end
        %sprawdzenie czy conajmniej jedno wejscie jest inne niz 0                   
        ile_nic = sum(eq([temp_val1 temp_val2],0));
        if ile_nic < 2
            dodaj_ok = 1;
        else
              dodaj_ok = 0;
        end              
    end
               
    %wybranie aktualnie kliknietego - input3
    if IleWejsc >= 3
        temp_val3 = get(handles.WeLiBox3,'Value');
        temp_str = get(handles.WeLiBox3,'String');
        str3 = temp_str{temp_val3};
        if strcmp(str3,'nic')
            temp_val3 = 0;
        end
       
        %sprawdzenie czy conajmniej jedno wejscie jest inne niz 0                  
        ile_nic = sum(eq([temp_val1 temp_val2 temp_val3],0));
        if ile_nic < 3
            dodaj_ok = 1;
        else
            dodaj_ok = 0;
        end
    end    
               
    if IleWejsc == 4
        temp_val4 = get(handles.WeLiBox4,'Value');
        temp_str = get(handles.WeLiBox4,'String');
        str4 = temp_str{temp_val4};
        if strcmp(str4,'nic')
            temp_val4 = 0;
        end
       
        %sprawdzenie czy conajmniej jedno wejscie jest inne niz 0
        ile_nic = sum(eq([temp_val1 temp_val2 temp_val3 temp_val4],0));
        if ile_nic < 4
            dodaj_ok = 1;
        else
            dodaj_ok = 0;
        end
    end               

    %pobranie wartosci z okna wyjsc
    %wybranie aktualnie kliknietego
    temp_val5 = get(handles.WyLiBox1,'Value');
    temp_str = get(handles.WyLiBox1,'String');
    str5 = temp_str{temp_val5};
    if strcmp( str5,'nic')
        temp_val5 = 0;
    end            
                
    if IleWyjsc == 2
        temp_val6 = get(handles.WyLiBox2,'Value');
        temp_str = get(handles.WyLiBox2,'String');
        str6 = temp_str{temp_val6};
        if strcmp( str6,'nic')
            temp_val6 = 0;
        end

        %sprawdzenie czy conajmniej jedno wejscie jest inne niz 0                   
        ile_nic = sum(eq([temp_val5 temp_val6],0));
        if ile_nic < 2
            dodaj_ok = 1;
        else
            dodaj_ok = 0;
        end                
    end 
            
    %generacja stringow  do listbox1 pod warunkiem dodaj_ok (odpowiednia
    %ilosc wejsc/wyjsc bez wartosci "nic"
    if dodaj_ok
        if strcmp(str1,'nic')
           str_part1 = [];
           %str_part1_con = [];
        else
           str_part1 = ([ fls.input(1).name jest_text str1]);
           temp_antecedent = temp_val1;
        end


        if IleWejsc < 2 || strcmp(str2,'nic')
            str_part2 = [];
            str_part1_con = [];
        else
            str_part2 = ([fls.input(2).name jest_text str2]);
            temp_antecedent = [temp_val1 temp_val2];
            if Conn == 1
                str_part1_con = i_text;
            elseif Conn == 2
                str_part1_con = '" lub "';
            end
        end 

        if IleWejsc < 3 || strcmp(str3,'nic')
            str_part3 = [];
            str_part2_con = [];
        else
            str_part3 = ([fls.input(3).name jest_text str3]);
            temp_antecedent = [temp_val1 temp_val2 temp_val3];   
            if Conn == 1
                str_part2_con = i_text;
            elseif Conn == 2
                str_part2_con = '" lub "';
            end
        end
        
        if IleWejsc < 4 || strcmp(str4,'nic')
            str_part4 = [];
            str_part3_con = [];
        else
            str_part4 = ([fls.input(4).name ' "' jest_text '" ' str4]);
            temp_antecedent = [temp_val1 temp_val2 temp_val3 temp_val4];   
            if Conn == 1
            	str_part3_con = i_text;
            elseif Conn == 2
            	str_part3_con = '" lub "';
            end                               
        end

        if strcmp(str5,'nic')
            str_part5 = [];
        else
            str_part5 = ([ fls.output(1).name ' "' jest_text '" ' str5 ' "']);
            temp_consequent = temp_val5;
        end

        if IleWyjsc == 2 
            if  strcmp(str6,'nic')
                str_part6 = [];   
            else
                str_part6 = ([fls.output(2).name jest_text str6]);
                temp_consequent = [temp_val5 temp_val6];
            end
        else             
            str_part6 = [];
        end
        
        baz_reg_txt = ([jezeli_text str_part1 str_part1_con str_part2 str_part2_con str_part3 str_part3_con str_part4 wtedy_text str_part5 str_part6]);

        fls.rule(lb1_dl+1).antecedent = temp_antecedent;
        fls.rule(lb1_dl+1).consequent = temp_consequent;
        fls.rule(lb1_dl+1).weight = 1;
        fls.rule(lb1_dl+1).connection = Conn;
        writefis(fls,nazwa);
        readfis(nazwa);
        baz_reg_i = lb1_dl;

        %stara baza zeby nowej nei wymazywalo
         old_baz_reg_str = get(handles.listbox1,'String');
         baz_reg_str(1:baz_reg_i)= cellstr(old_baz_reg_str);
         
         regBuffer1 = string(baz_reg_txt);
         
         baz_reg_str(baz_reg_i+1) = cellstr(horzcat(regBuffer1{:}));
         
         set(handles.listbox1,'Value',length(baz_reg_str));
         set(handles.listbox1,'String',baz_reg_str');

        guidata(hObject, handles);
    end


