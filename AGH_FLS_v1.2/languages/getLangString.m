function [output] = getLangString(key,lang)
%GETLANGSTRING Pobiera tekst do wyświetlenia
%   output - pobrany string
%   key - klucz stringa
%   lang - jezyk

    try
        switch lang
            case 'pl'
                load('polski','langPL');
                output = langPL(key);
            case 'en'
                load('english','langEN');
                output = langEN(key);
            otherwise
                load('polski','langPL');
                output = langPL(key);
        end
        
        if iscell(output)
            output = char(output);
        end
    catch
        output = '???';
    end

end

