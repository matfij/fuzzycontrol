%   Tworzy MAP'y, czyli kontenery stringow uzywane w Toolbox'ie
% 
%   Wszystkei stringi powinny byc wyswietlane posrednio przez funkcje
%   odczytujace stringi z MAP.
% 
%   Obecne j�zyki to polski i angielski.
%   Mo�na doda� kolejne wype�niaj�c macierz 'tab' kolejn� kolumn� i metodami
%   tworz�cymi MAPy.
% 
%   tab = {klucz, 'polski', 'angielski'}
%   langPL - mapa ze slowami polskimi
%   langEN - mapa ze slowami angielskimi
% 
%   Ten plik nie jest potrzebny do funkcjonowania Toolbox'a, natomiast
%   dane przez niego tworzone ju� tak.

%d�ugie stringi
str1 =          'Praca magisterska                                                                 ';
str1 = [str1;   '   Autor:        Marcin Mik�as                                                    '];
str1 = [str1;   '   Specjalnosc:  Automatyka i Metrologia                                          '];
str1 = [str1;   '   Temat:        Oprogramowanie s�u��ce do bada� nad regulatorami rozmytymi typu 2'];
str1 = [str1;   '   Promotor:     dr inz. Ireneusz Dominik                                         '];



%macierz do MAP
tab = {
    %   przymiotnik danego jezyka w danym jezyku
    0   'polski'    'English'
   
%IT2FLS main gui
    10  'J�zyk'         'Language'
    11  'polski'        'Polish'
    13  'angielski'     'English'
    14  'Widok'         'View'
    15  'Powierzchnia sterowa�' 'Control Surfaces'
    16  'Podgl�d regu�'         'Rules view'
    17  'Edycja'                'Modify'
    18  'Dodaj/Usu� zmienn�'    'Add/Remove Variable'
    19  'Plik'          'File'
    20  'Import'        'Import'
    21  'Z worspace'    'From workspace'
    22  'Z pliku'       'From File'
    23  'Eksport'       'Export'
    24  'Dodaj metod� defuzyfikacji' 'Add defuzzification method'
    25  'Zamknij'       'Close'
    26  'Przekr�j'      'zSlice'
    27  'Powierzchnia'  'Surface'
    28  'Wej�cia/Wyj�cia'   'Inputs/Outputs'
    29  'Baza regu�'        'Rules base'
    30  'Liczba wej��: '    'Number of inputs: '
    31  'Liczba wyj��: '    'Number of outputs: '
    32  'Ustawienia FIS'    'FIS Settings'
    33  'Metoda iloczynu'   'AND method'
    34  'Metoda sumowania'  'OR method'
    35  'Metoda implikacji zbior�w'     'Implication'
    36  'Metoda skupiania zbior�w'      'Aggregation'
    37  'Metoda redukcji'   'Reduction method'
    38  'Defuzyfikacja'     'Defuzzification'
    39  'Obecny FIS'        'Current FIS'
    40  'Nazwa FIS'         'FIS name'
    41  'Typ FIS'           'FIS type'
    42  'General fuzzy'     'General fuzzy'
    43  'Info'              'Info'
    44  'Do workspace'      'To workspace'
    45  'Do pliku'          'To file'
    46  'Liczba regu�: '    'Number of rules: '
    47  'Brak regu�!'        'No rules'
    48  'Tylko dla dw�ch wej�� i jednego wyj�cia'   'Only for two inputs and one output'
    
    
    %dialog'i
    70  {str1},             {str1}
    71  'Informacja'        'Information'
    72  'Zapisa� zmiany do pliku?'	'Save changes to file?'
    73  'Tak'               'Yes'
    74  'Nie'               'No'
    75  'Anuluj'            'Cancel'
    76  'AGH FIS'           'AGH FIS'
    77  'Wczytano: '        'Loaded: '
    78  'Wczytanie si� nie powiod�o.'   'Loading failed.'
    79  'Wczytanie si� powiod�o.'       'Loading succeeded.'
    80  'Zapisano: '        'Saved: '
    81  'Zapis si� nie powi�d�.'        'Saving failed.'
    82  'Zapis si� powi�d�.'            'Saving succeeded.'
    83  'Generuj kod do IDE'     'Generate code to IDE'
    
   
%dawanie funkcji gui
    100 'Dodawanie i usuwanie zmiennych'     'Adding and removing variables'
    101 'Wej�cia'       'Inputs'
    102 'Wyj�cia'       'Outputs'
    103 'Dodaj'         'Add'
    104 'Usu�'          'Remove'
    105 'Zapisz'        'Save'
    
%AS gui
    120 'Okre�l wielko�� p�aszczyzny sterowania.'   'Determine the size of control surface'
    121 'Zalecana wielko��: 2^n.'   'Recommended size: 2^n.'
    122 'Rozdzielczo��:'    'Resolution:'
    123 'Generuj kod'               'Generate'
    124 'Otw�rz Konfiguracj�'       'Open the configuration'
    125 'Rozdzielczo�� musi by� liczb�.' 'Resulotuion must be a number.'
    126 'Rysowanie powierzchni si� nie uda�o'   'Surface drawing failed'
    127 'Otwieranie Simulinka'      'Opening Simulink'
    128 'Generowanie kodu'          'Code generating'
    129 'Otwieranie katalogu'       'File opening'
    130 'oraz okres pr�bkowania.'   'and sampling period.'
    131 'Czas pr�bkowania:'         'Sampling time:'
    132 'Generowanie kodu'          'Code generation'
    133 'P�aszczyzna sterowania'    'Control surface'
    134 'Rysuj'                     'Draw'
    135 'Nastawy regulatora'        'Control parameters'
    136 'Pozosta�e parametry'       'Other parameters'
    137 'Nasycenie cz�onu ca�kuj�cego:'         'Integrator part saturation:'
    138 'Dzia�anie'                             'Action'
    139 'Otw�rz konfiguracj�'                   'Open the configuration'
    140 'Zapis do workspace si� nie powi�d�'    'Saving to workspace failed'
    141 'Zapisz'                                'Save'
    142 'Otwieranie konfiguracji'               'Config opening'
    143 'Generowanie kodu nie powiod�o si�'     'Code generating failed'
    144 'Otwarcie konfiguracji nie powiod�o si�'    'Opening the config failed'
    
    
    
%Wej_ed gui
    170 'Edycja wej��/wyj��'    'Modify input/output'
    171 'Wej�cia'               'Inputs'
    172 'Rysuj'                 'Draw'
    173 'Wyj�cia'               'Outputs'
    174 'Funkcja przynale�no�ci'    'Membership function'
    175 'Dodaj'                 'Add'
    176 'Usu�'                  'Remove'
    177 'Wyb�r zSlice'          'zSlice selection'
    178 'Obecna zmienna'        'Current variable'
    179 'Nazwa'                 'Name'
    180 'Zakres'                'Range'
    181 'Wy�wietlany zakres'    'Display range'
    182 'Obcna funkcja przynale�no�ci'  'Current membership function'
    183 ''                 ''
    184 'Parametry'             'Parameters'
    185 'Typ'                   'Type'
    186 'Menu'                  'Menu'
    187 'Info'                  'Info'
    188 'Zamknij'               'Close'


%Baza_regul gui
    220 'Baza regu�'            'Rules base'
    221 'Je�eli...'             'If...'
    222 '...to...'              '...then...'
    223 ' jest...'              ' is...'
    224 'Dodaj regu��'          'Add the rule'
    225 'Usu� regu��'           'Remove the rule'
    226 '" i "'                 '" and "'
    227 'Je�eli "'              'If "'
    228 '" jest "'              '" is "'
    229 '" wtedy "'             '" then "'
    

%Pod_reg gui
    260 'Podgl�d regu�'         'Rules view'
    261 'Obecne warto�ci'       'Current values'
    262 'Warto�� wyj�cia'       'Output value'
    263 'Kt�ry przekr�j'        'Which zSlice'
    264 'Warto�ci wej��'        'Input values'
    265 'Wej�cie 1'             'Input 1'
    266 'Wej�cie 2'             'Input 2'
    267 'Wej�cie 3'             'Input 3'
    268 'Wej�cie 4'             'Input 4'
    269 'Info'                  'Info'
    270 'Zamknij'               'Close'
    271 'Menu'                  'Menu'
    
%General_ed gui    
    300 'zSlice'                'zSlice'
    301 'Liczba przekroj�w'     'zSlice number'
    302 'Ilosc przekrojow ma, znaczacy wplyw na predkosc komputacji wyjscia.'...
        'Number of zslices has significant influence on speed of output computing'

%Surf gui
    320 'Podgl�d powierzchni sterowa�'  'Control surface view'
    321 'Do workspace'          'To workspace'
    322 'Menu'                  'Menu'
    323 'X(Wej�cie 1)'          'X(Input 1)'
    324 'Wyb�r wej�cia 1'       'Input 1 selection'
    325 'Y(Wej�cie 2)'          'Y(Input 2)'
    326 'Wyb�r wej�cia 2'       'Input 2 selection'
    327 'Z(Wyj�cie 1)'          'Z(Output 1)'
    328 'Wyj�cie'               'Output'
    329 'G�sto�� siatki X'      'Mesh X density'
    330 'G�sto�� siatki Y'      'Mesh Y density'

    
%Dodawanie_funkcji gui
    350 'Dodawanie funkcji'     'Adding function'
    351 'Dodawanie metod defuzyfikacji' 'Adding defuzzification methods'
    352 'Nazwa'                 'Name'
    353 '�cie�ka'               'Path'
    354 'Tre�� funkcji'         'Functions content'
    355 'Zmienne wej�ciowe funkcji �redniowa�onej:'    'Input variables average weighted function'
    356 'c - centra'            'c - centers'
    357 's - rozrzut'           's - spread'
    358 'h - delta'             'h - delta'
    359 'Dodaj'                 'Add'
    360 'Anuluj'                'Cancel'
    
    
    
    };
clear str1;

langPL = containers.Map( tab(:,1), tab(:,2) );
save('polski','langPL');

langEN = containers.Map( tab(:,1), tab(:,3) );
save('english','langEN');

clear langPL langEN tab;

