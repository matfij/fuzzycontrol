function varargout = codeGenerationAS(varargin)
% CODEGENERATIONAS MATLAB code for codeGenerationAS.fig
%      CODEGENERATIONAS, by itself, creates a new CODEGENERATIONAS or raises the existing
%      singleton*.
%
%      H = CODEGENERATIONAS returns the handle to a new CODEGENERATIONAS or the handle to
%      the existing singleton*.
%
%      CODEGENERATIONAS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in CODEGENERATIONAS.M with the given input arguments.
%
%      CODEGENERATIONAS('Property','Value',...) creates a new CODEGENERATIONAS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before codeGenerationAS_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to codeGenerationAS_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help codeGenerationAS

% Last Modified by GUIDE v2.5 27-Nov-2018 13:07:02

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @codeGenerationAS_OpeningFcn, ...
                   'gui_OutputFcn',  @codeGenerationAS_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before codeGenerationAS is made visible.
function codeGenerationAS_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to codeGenerationAS (see VARARGIN)

% Choose default command line output for codeGenerationAS
handles.output = hObject;


%obsluga argumentow
if nargin > 0
    if ~isempty(varargin)
        nazwa = char(varargin(1));
        fls = readfis(nazwa);
        handles.nazwa = nazwa;
    else
        fls = readfis('bez_nazwy');
        handles.nazwa = 'bez_nazwy';
        nazwa = handles.nazwa;
    end
else
    fls = readfis('bez_nazwy');
    handles.nazwa = 'bez_nazwy';
    nazwa = handles.nazwa;    
end

if nargin > 1
    if ~isempty(varargin)
        lang = char(varargin(2));
        handles.lang = lang;
    else
        lang = 'pl';
        handles.lang = lang;
    end
else
    lang = 'pl';
    handles.lang = lang;
end

if nargin > 2
    if ~isempty(varargin)
        redMethod = char(varargin(3));
        handles.redMethod = redMethod;
    else
        redMethod = 'EKM';
        handles.redMethod = redMethod;
    end
else
    redMethod = 'EKM';
	handles.redMethod = redMethod;
end
    
%stringi w gui
setGuiStrings(handles);


% Update handles structure
guidata(hObject, handles);

% UIWAIT makes codeGenerationAS wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = codeGenerationAS_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


%% Generuj kod
% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    lang = handles.lang;
    
try
    %stworzenie waitBar
    waitBarGen = waitbar(0,'.');
 
    %otwarcie modelu w simulinku
    waitbar(0.33, waitBarGen, getLangString(127,lang));
    fuzzControlSys;
    
    %wybor ilosci wejsc regulatora - generowanie kodu
    waitbar(0.67,waitBarGen,getLangString(128,lang));
    switch length(fls.input)
        case 2
            generatedfiles = plcgeneratecode('fuzzControlSys/FuzCon');
    end
    
    %otwarcie katalogu
    waitbar(1,waitBarGen,getLangString(129,lang));
    winopen([cd '\plcsrc']);
    close(waitBarGen);
    
    %powierdzenie wygenerowania kodu
    msgbox([getLangString(80,lang) generatedfiles]);
    
catch
    msgbox(getLangString(143,lang),'Error','error');
    close(waitBarGen);
end


%% Otw�rz konfiguracje
% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    nazwa = handles.nazwa;
    fls = readfis(nazwa);
    lang = handles.lang;
    
try
    %stworzenie waitBar
    waitBarGen = waitbar(0,'.');
 
    %otwarcie modelu w simulinku
    waitbar(0.33, waitBarGen, getLangString(127,lang));
    fuzzControlSys;
    
    %wybor ilosci wejsc regulatora i otwarcie konfiguracji
    waitbar(0.67,waitBarGen,getLangString(142,lang));
    switch length(fls.input)
        case 2
            plcopenconfigset('fuzzControlSys/FuzCon');
    end
    
    close(waitBarGen);
    
catch
    msgbox(getLangString(144,lang),'Error','error');
    close(waitBarGen);
end



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


%%Funkcja podstawiajaca stringi
function setGuiStrings(handles)
lang = handles.lang;

set(handles.figure1,'Name', getLangString(132,lang));

set(handles.uipanel1,'Title',getLangString(133,lang));
 set(handles.text3,'String',getLangString(120,lang));
 set(handles.text4,'String',getLangString(121,lang));
 set(handles.text2,'String',getLangString(122,lang));
 set(handles.pushbutton3,'String',getLangString(134,lang));

set(handles.uipanel2,'Title',getLangString(135,lang));
  set(handles.text13,'String',getLangString(137,lang));
  set(handles.pushbutton4,'String',getLangString(141,lang));

set(handles.uipanel4,'Title',getLangString(136,lang));
 set(handles.text5,'String',getLangString(131,lang));
 set(handles.pushbutton5,'String',getLangString(141,lang)); 

set(handles.uipanel5,'Title',getLangString(138,lang));
 set(handles.pushbutton1,'String',getLangString(123,lang));
 set(handles.pushbutton2,'String',getLangString(124,lang));




function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit4_Callback(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit4 as text
%        str2double(get(hObject,'String')) returns contents of edit4 as a double


% --- Executes during object creation, after setting all properties.
function edit4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

%% tworzenie p�aszczyzny
% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
nazwa = handles.nazwa;
fls = readfis(nazwa);
lang = handles.lang;
    
try
    if isempty(fls.rule)
        msgbox(getLangString(126,lang),'Error','error');
        return
    end
    
    n = str2double( get(handles.edit1,'String') );
    
    if isempty(n)
        msgbox(getLangString(125,lang),'Error','error');
        return
    end
    
    
    %powiwszchnia sterowania
    surf = controlSurface(fls,handles.redMethod, n);
    assignin('base','sf_surf',surf);
    
    %min/max wielko�� wej�cia
    r = fls.input.range;
    assignin('base','sf_r',single(r) );
    
    
catch
    msgbox(getLangString(126,lang),'Error','error');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    %odczytanie liczb z okien
    Ke      = str2double( get(handles.edit3,'String') );
    Kd      = str2double( get(handles.edit5,'String') );
    beta    = str2double( get(handles.edit4,'String') );
    alpha   = str2double( get(handles.edit6,'String') );
    sat     = str2double( get(handles.edit7,'String') );
    limit   = [str2double( get(handles.edit10,'String') ), str2double( get(handles.edit11,'String') )];
    
    if isempty(Ke) || isempty(Kd) || isempty(beta) || isempty(alpha) || isempty(sat) || isempty(limit)
        msgbox(getLangString(125,lang),'Error','error');
        return
    end
    
    %zapis nastaw do workspace
    assignin('base','sf_Ke',single(Ke));
    assignin('base','sf_Kd',single(Kd));
    assignin('base','sf_beta',single(beta));
    assignin('base','sf_alpha',single(alpha));
    assignin('base','sf_sat',single(sat));
    assignin('base','sf_limit',single(limit));
    
catch
    msgbox(getLangString(140,lang),'Error','error');
    
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    %odczytanie liczb z okien
    Tp      = str2double( get(handles.edit2,'String') );
    
    if isempty(Tp)
        msgbox(getLangString(125,lang),'Error','error');
        return
    end
    
    %zapis nastaw do workspace
    assignin('base','sf_Tp',Tp);
    
catch
    msgbox(getLangString(140,lang),'Error','error');
    
end



function edit10_Callback(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit10 as text
%        str2double(get(hObject,'String')) returns contents of edit10 as a double


% --- Executes during object creation, after setting all properties.
function edit10_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit10 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit11_Callback(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit11 as text
%        str2double(get(hObject,'String')) returns contents of edit11 as a double


% --- Executes during object creation, after setting all properties.
function edit11_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit11 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
